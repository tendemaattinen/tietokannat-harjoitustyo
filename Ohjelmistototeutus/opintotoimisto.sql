# Query:
# 1. Käyttäjälle tulostettava kuvaus
# 2. käyttäjäryhmät
# 3. Muuttujien kuvaus käyttäjälle
# 4. SQL-lauseke
#

Query:
{Listaa kaikkien oppilaiden tiedot.}
{admin,sihteeri,opettaja}
{}
{SELECT * FROM Opiskelijat}

Query:
{Listaa kaikkien opettajien tiedot.}
{admin,sihteeri,opettaja,oppilas}
{}
{SELECT * FROM Opettajat}

Query:
{Listaa kaikki kurssit.}
{admin,sihteeri,opettaja,oppilas}
{}
{SELECT Kurssit.Kurssitunnus, Kurssit.Nimi, Kurssit.Kurssikuvaus,
Kurssit.Opintopisteet, Kurssit.Vastuuhenkilö, Kurssit.Alkamisaika,
Kurssit.Päättymisaika, Kurssit.Luentosali, Kirjat."Kirjan nimi" AS Kurssikirja, Oppilaitos.Nimi AS Oppilaitos
 FROM Kurssit
 INNER JOIN Kirjat ON Kirjat.KirjaID = Kurssit.Kurssikirja
 INNER JOIN Oppilaitos ON Oppilaitos.LaitosID = Kurssit.Oppilaitos}

Query:
{Listaa kaikki kursseille ilmottautumiset.}
{admin,sihteeri,opettaja}
{}
{SELECT * FROM Osallistuu}

Query:
{Listaa oppilaan arvosanat.}
{admin,sihteeri,oppilas,opettaja}
{Opiskelijatunnus}
{SELECT Kurssit.Nimi AS Kurssi, Suoritukset.Arvosana FROM Suoritukset
 INNER JOIN Kurssit ON Kurssit.Kurssitunnus = Suoritukset.Kurssitunnus WHERE Opiskelijatunnus LIKE "\1\"}

Query:
{Listaa oppilaan kaikki kurssit.}
{admin,sihteeri,oppilas,opettaja}
{Opiskelijatunnus}
{SELECT Osallistuu.Opiskelijatunnus, Kurssit.Nimi AS Kurssi FROM Osallistuu
 INNER JOIN Kurssit ON Kurssit.Kurssitunnus = Osallistuu.Kurssitunnus WHERE Opiskelijatunnus LIKE "\1\"}

Query:
{Listaa opettajan kaikki kurssit.}
{admin,sihteeri,opettaja}
{Tunnus}
{SELECT Opettajat.Tunnus, Kurssit.Nimi AS Kurssi FROM Kurssit
 INNER JOIN Opettajat ON Opettajat.Tunnus = Kurssit.Vastuuhenkilö WHERE Tunnus LIKE "\1\"}

Query:
{Listaa oppilaan opintopisteet.}
{admin, sihteeri,opettaja,oppilas}
{Opiskelijatunnus}
{SELECT Suoritukset.Opiskelijatunnus, SUM(Kurssit.Opintopisteet) AS Pisteet FROM Suoritukset
 INNER JOIN Kurssit ON Kurssit.Kurssitunnus = Suoritukset.Kurssitunnus
 WHERE Opiskelijatunnus LIKE "\1\"}

Query:
{Listaa kaikkien työntekijöiden tiedot.}
{admin,sihteeri,opettaja,oppilas}
{}
{SELECT * FROM Tyontekijat}

 Query:
{Listaa kaikki tapahtumat.}
{admin,sihteeri,opettaja,oppilas}
{}
{SELECT * FROM Tapahtumat}

Query:
{Listaa kaikki kirjat.}
{admin,sihteeri,opettaja,oppilas}
{}
{SELECT * FROM Kirjat}

Query:
{Listaa kaikki oppilaitokset.}
{admin,sihteeri,opettaja,oppilas}
{}
{SELECT * FROM Oppilaitos}

Query:
{Syötä vapaamuotoinen SELECT-kysely.}
{admin}
{SQLite3 SELECT-lauseke}
{\1\}
