# INSERT_ALTER.PY
#

from query import printQueryResults
from class_def import debug_mode


# Yleiskäyttöinen aliohjelma, joka suorittaa SQL-INSERT-
# komennon tietokantaa vastaan. Funktio ottaa INSERTin
# parametrina merkkijonona. Toinen parametri on sqlite3.
# connection-tyyppinen objekti.
# 
def executeSQLInsert(sql_cmd, db_connection):
    cursor = db_connection.cursor()    
    try:
        cursor.execute(sql_cmd)
        db_connection.commit()
    except Exception as error: 
        db_connection.rollback() 
        print("\nVIRHE: Tietueen lisäys ei onnistunut.")
        print("SQLite:", error,)
    else:
        print("\nLisäys suoritettu.")

    return


# Aliohjelmaa hyödynnetään räätälöidyissä insert* 
# funktioissa. Palauttaa seuraavan vapaana olevan
# pääavainarvon, kun parametrina on annettu attribuutin
# nimi, taulu, ja sqlite3.connection.cursor-objekti.
# Huom: tulisi päivittää seuraavasti:
# - Ei tarvita attribute-parametria, koska se voidaan selvittää
#   taulun nimen avulla kyselyllä.
# - Tällöin voidaan tukea n-osaisia avaimia ja palauttaa aina
#   n-pituinen lista, jossa uusi avainarvoyhdistelmä (1..N)
# 
def fetchNextPrimaryKeyValue(attribute, table, cursor):
    cursor.execute("SELECT " + attribute + " FROM " + table)
    lite3_output = cursor.fetchall()
    # [-1] --> viimeinen alkio, [-1][0] --> 2-ulotteisen
    # listan 1. alkio.
    return (int(lite3_output[-1][0]) + 1)


# Aliohjelman avulla voidaan suorittaa UPDATE ja DELETE
# SQL-komentoja mielivaltaista tietokantaa vastaan.
# 
def alterRecord(user_group, db_connection):
    if (user_group == "oppilas") or (user_group == "tyontekija"):
        print("Käyttäjtunnuksella ei ole tällä hetkellä muokkausoikeuksia.")    
        return

    cursor = db_connection.cursor()

    print("\nValitse taulu, jonka tietoja muutetaan:\n")

    cursor.execute("SELECT name AS 'Taulu' FROM sqlite_master WHERE type = 'table'")
    printQueryResults(cursor.fetchall(), cursor.description)
    table = str(input("\n> "))

    print("\nValitse kohteena oleva tietue:\n")

    cursor.execute("SELECT * FROM " + table)
    printQueryResults(cursor.fetchall(), cursor.description)

    print("\nSyötä ID päivittääksesi tietueen.")
    print("Syötä P ID poistaaksesi tietueen.")
    print("Syötä (P) ID ID ... kun kysessä on moniosainen avain.")

    # .split palauttaa listan; kts. Python funktiot.
    ID_set = input("(ID) > ").split(" ")

    if ID_set[0] == "P": 
        # Lähetetään leikkaus [1:] -> alkiot 1, 2, 3...
        # Koska aliohjelma ei odota muuta kuin ID-settiä.
        deleteRecord(ID_set[1:], table, db_connection)        
    else:
        updateRecord(ID_set, table, db_connection)

    input()
    return


# Aliohjelma suorittaa varsinaisen DELETE-komennon tietokantaa
# vastaan. Ottaa parametrina listan, jossa ovat poistettavan
# tietueen pääavainattribuuttien arvot, esim. [1, 'P']
# Funktio selvittää tämän tiedon perusteella tarvistemansa 
# DELETE-lauseen muut parametrit. 
# 
def deleteRecord(ID_set, table, db_connection):
    p_key_values = [] # pääavaimet

    # Haetaan taulun SQLite3 schema. pragma tuottaa
    # 2-ulotteisen listan [attribuutin nimi][ominaisuus] 
    cursor = db_connection.cursor()
    cursor.execute("pragma table_info(" + table + ")")

    i = 0
    for column in cursor.fetchall():
        if column[5] == 1: # Onko pääavainatribuutti?
            p_key_values.append([column[1], None])
            if "VARCHAR" in column[2]:
                p_key_values[-1][1] = "'" + ID_set[i] + "'"
            else: 
                p_key_values[-1][1] = ID_set[i]
            i += 1

    # p_key_values on nyt 2-ulotteinen lista, jossa on:
    # [p-avain attribuutin nimi] [arvo] 
    # esim. [[PelaajaID, 6], [JoukkueID, 1]]

    # Voidaan rakentaa DELETE-käsky:
    sql_cmd = "DELETE FROM " + table + " WHERE "
    for i in range(len(p_key_values)):
        if i > 0:
            sql_cmd += " AND "
        sql_cmd += p_key_values[i][0] + "=" + p_key_values[i][1]

    if (debug_mode):        
        print("\nDEBUG (deleteRecord):", sql_cmd)

    try:
        cursor.execute(sql_cmd)
        db_connection.commit()
    except Exception as error: 
        db_connection.rollback() 
        print("\nVIRHE: Tietuetta ei voitu poistaa.")
        print("SQLite:", error)
    else:
        print("\nTietue poistettu.")

    return


# Kuten deleteRecord, aliohjelma suorittaa varsinaisen UPDATE-
# käskyn tietokantaa vastaam. Toimintaperiaate on vastaava kuin
# deletessä, mutta UPDATE-käskyn useiden osien vuoksi 
# joudutaan se rakentamaan vastaavasti useammin tövaihein.
# Saa parametrina kohdetietueen pääavainarvojoukon listana.
# Aliohjelma rakentaa UPDATE-käskyn tämän tiedon pohjalta.
#
def updateRecord(ID_set, table, db_connection):
    p_key_values = [] # pääavainarvot
    cursor = db_connection.cursor()

    # kts. deleteRecord
    cursor.execute("pragma table_info(" + table + ")")
    columns = cursor.fetchall()

    i = 0
    for column in columns:
        if column[5] == 1:
            if "INTEGER" in column[2]: 
                p_key_values.append([column[1], ID_set[i]])
            else:
                p_key_values.append([column[1], "'" + ID_set[i] + "'"])                            
            i += 1

    # Pitää selvittää ensin, mitkä ovat kohteena olevan
    # tietueen nykyiset arvot, jotta niitä voidaan ehdottaa 
    # käyttäjälle oletusarvoina, kun pyydetään syötteitä. 
    sql_cmd = "SELECT * FROM " + table + " WHERE "
    for i in range(len(p_key_values)):
        if i > 0:
            sql_cmd += " AND "
        sql_cmd += p_key_values[i][0] + "=" + p_key_values[i][1]

    if (debug_mode):        
        print("\nDEBUG (updateRecord):", sql_cmd)

    cursor.execute(sql_cmd)
    old_values = cursor.fetchone() 

    if (debug_mode):        
        print("DEBUG (deleteRecord):", old_values)

    print("\nSyötä kullekin attribuutille uusi arvo tai hyväksy nykyinen painamalla [enter].")

    # Voidaan alkaa rakentaa päivitettävää joukkoa.
    # Kysytään käyttäjältä kunkin kohdalla uusi arvo.
    # Pelkällä enterillä käytetään vanhaa arvoa.
    sql_cmd = "UPDATE " + table + " SET "
    for i in range(len(columns)):
        # Tulostetaan vanha arvo
        new_value = input(columns[i][1] + " (" + str(old_values[i]) + ") : ")     
        
        if new_value == "": # -> pelkkä enter
            new_value = str(old_values[i])
     
        # Lisätään pilkku eteen vasta toisesta kierroksesta lukien.
        # SET ATTR1=JokuArvo, ATTR2=JokuArvo2, ...
        if i > 0: 
            sql_cmd += ", "

        # Muistetaan lisätä '-meerkit, jos ei numeerinen arvo.
        if ("INTEGER" in columns[i][2]): 
            sql_cmd += columns[i][1] + "=" + new_value
        else:
            sql_cmd += columns[i][1] + "='" + new_value + "'"
        
    # SET-lauseke on valmis. Tarvitaan WHERE-osio, joka kertoo,
    # mikä tietue päivitetään. Käytetään muistiin laitettuja 
    # pääavaianrvoja, koska ne ainakin dentifioivat haluamamme
    # tietueen oikein. Nyt nähdään p_key_values:n tarkoitus.
    sql_cmd += " WHERE" 
    for i in range(len(p_key_values)):
        if i > 0: # Ei ANDia 1. Attr=Arvo:n eteen...
            sql_cmd += " AND"
        sql_cmd += " " + p_key_values[i][0] + "=" + p_key_values[i][1]

    if (debug_mode):        
        print("\nDEBUG (deleteRecord):", sql_cmd)
      
    try:
        cursor.execute(sql_cmd)
        db_connection.commit()
    except Exception as error: 
        db_connection.rollback() 
        print("\nVIRHE: Tietuetta ei voitu päivittää.")
        print("SQLite:", error)
    else:
        print("\nTietue päivitetty.")      

    return



def insertRecordGame(db_connection):
    cursor = db_connection.cursor()

    newID = fetchNextPrimaryKeyValue("OtteluID", "Ottelu", cursor)

    print("\nValitse kotijoukkue ja vierasjoukkue:")

    cursor.execute("SELECT JoukkueID, Nimi FROM Joukkue")
    printQueryResults(cursor.fetchall(), cursor.description)
    home_team = input("\nKotijoukkue (ID): ")
    visiting_team = input("Vierasjoukkue (ID): ")

    category = '"' + input("Valiste otteluluokka (V/E): ") + '"'
    score = '"' + input("Ottelun tulos: ") + '"'
    date = input("Päiväys (pp.kk.vvvv): ")
    temp = date.split(".")
    date = '"' + temp[2] + "-" + temp[1] + "-" + temp[0] + '"'
 
    print("Valitse pelipaikka:")
    cursor.execute("SELECT PelipaikkaID FROM Pelipaikka")
    printQueryResults(cursor.fetchall(), cursor.description)

    location = '"' + input("\nPelipaikka (ID): ") + '"'

    # Rakennetaan SQL-lauseke:
    sql_cmd = "INSERT INTO Ottelu VALUES ("
    sql_cmd += str(newID) + "," + home_team + "," + visiting_team + "," + category + "," + score + "," + date + "," + location + ")"

    if (debug_mode):        
        print("DEBUG (insertRecordGame):", sql_cmd)

    executeSQLInsert(sql_cmd, db_connection)

    return


def insertRecordPlayer(db_connection):
    cursor = db_connection.cursor()
    newID = fetchNextPrimaryKeyValue("PelaajaID", "Pelaaja", cursor)

    print("\nSyötä pelaajan tiedot:\n")
    name = '"' + input("Nimi: ") + '"'
    number = str(input("Pelinumero: "))
    address = '"' + input("Osoite: ") + '"'
    phone_num = '"' + str(input("Puhelinnumero: ")) + '"'
    score = str(input("Pisteet: "))
    print("Joukkue:")

    cursor.execute("SELECT JoukkueID, Nimi FROM Joukkue")
    printQueryResults(cursor.fetchall(), cursor.description)
    team = str(input("\n(ID) > "))

    # Rakennetaan SQL-lauseke:
    sql_cmd = "INSERT INTO Pelaaja VALUES ("
    sql_cmd += str(newID) + "," + team + "," + name + "," + number + "," + score + "," + address + "," + phone_num + ")"

    if (debug_mode):
        print("DEBUG (insertRecordPlayer):", sql_cmd)

    executeSQLInsert(sql_cmd, db_connection)

    return


# Tleiskäyttöinen INSERT-komennon suorittava aliohjelma,
# joka lisää tietueen parametrina annetun nimsieen tauluun.
# Koska geneerinen, toimii minkä vain tietokannan kanssa, mutta
# ei siedä syötevirheitä eikä soaa kysyä tietoja "älykkäästi."
# 

def insertRecordGeneric(db_connection, table):
    columns = []

    # Esitetään käyttäjälle taulun tietueet helpottamaan
    # uuden lisäämistä.
    cursor = db_connection.cursor()
    cursor.execute("SELECT * FROM " + table)
    printQueryResults(cursor.fetchall(), cursor.description)

    print("\nSyötä uusi tietue:\n")

    # pragma palauttaa taulun scheman
    # kts. deleteRecord ja updateRecord.
    cursor = db_connection.cursor()
    cursor.execute("pragma table_info(" + table + ")")
    sql_cmd = "INSERT INTO " + table + " VALUES("

    # Emme saa kirjoittaa pilkkua 1. attr=arvo parin eteen,
    # joten kontrolloidaan millä kierroksella ollaan.
    first = True 
    # Edetään attribuutti kerrallaan.
    for column in cursor.fetchall():
        # column[1]:ssä on attribuutin nimi
        value = input(column[1] + ": ")
        if not("INTEGER" in column[2]): # Muistetaan '-merkit.
            value = "'" + value + "'"
        if (first):
            sql_cmd += value # Ei pilkkua.
            first = False # Ensi kierroksella kyllä.
        else:
            sql_cmd += ", " + value # Pilkku eteen, ei jälkeen.

    sql_cmd += ")"

    if (debug_mode):
        print("\nDEBUG (insertRecordGeneric):", sql_cmd)

    executeSQLInsert(sql_cmd, db_connection)

    return


